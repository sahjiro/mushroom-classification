# Mushroom classification

Le fichier exécutable est `script.py`.

L'ensemble de la démarche est expliquée dans le document PDF. S'il paraît vide, il faut le télécharger pour avoir un aperçu.

Les librairies nécessaires au fonctionnement du script sont : 
- sklearn
- pandas
- pickle
