import pandas as pd
from sklearn.preprocessing import LabelEncoder
import pickle

def predict(df):
    label_encoder = LabelEncoder()

    # Preparation de la dataFrame
    df = df.loc[:,df.columns!='veil-type']
    df['class'] = label_encoder.fit_transform(df['class'])   
    data_dummyVariables = pd.get_dummies(df)
    features = ['odor_n', 'odor_f', 'gill-size_b', 'gill-size_n', 'gill-color_b', 'bruises_t', 'bruises_f', 'spore-print-color_h', 'spore-print-color_n', 'spore-print-color_w', 'spore-print-color_k'] 
    X_test = data_dummyVariables[features]

    # Chargement du modèle random forest
    filename = 'randomForestClassifier.sav'
    loaded_model = pickle.load(open(filename, 'rb'))
    y_pred = loaded_model.predict(X_test)
    y_pred_str = label_encoder.inverse_transform(y_pred)
    return(y_pred_str)

if __name__=="__main__":
    df = pd.read_csv('evaluation.csv')
    true_y = df['class']
    y = predict(df)
    evaluate(y, true_y)